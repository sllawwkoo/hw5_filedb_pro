import fsPromise from "fs/promises";

interface News {
	title?: string;
	text?: string;
	id?: number;
}

export interface Schema {
	id: number;
	createDate: Date | string;
	title: string;
	text: string;
}


//Клас FileDB є сінглтоном через використання приватного статичного поля instance, яке містить єдиний екземпляр класу. У методі getInstance() перевіряється, чи вже існує екземпляр класу, і якщо ні, то створюється новий. Після цього метод повертає цей єдиний екземпляр. Таким чином, в програмі може існувати лише один екземпляр класу FileDB, що дозволяє забезпечити глобальний доступ до нього та уникнути непотрібного дублювання об'єктів.

class FileDB {
	private static instance: FileDB;
	private schema: { [key: string]: Schema };

	private constructor() {
		this.schema = {};
	}

	public static getInstance(): FileDB {
		if (!FileDB.instance) {
			FileDB.instance = new FileDB();
		}
		return FileDB.instance;
	}

	async registerSchema(tableName: string, schema: Schema): Promise<void> {
		try {
			this.schema = { ...this.schema, [tableName]: schema };
			await fsPromise.writeFile(
				`${tableName}.json`,
				JSON.stringify([], null, 2)
			);
		} catch (error) {
			console.log("Failed to save data:", error);
		}
	}

	private async readFile(tableName: string): Promise<Schema[]> {
		try {
			const data = await fsPromise.readFile(`${tableName}.json`, "utf8");
			return JSON.parse(data);
		} catch (error) {
			console.log("File does not exist or is empty.");
			return [];
		}
	}

	private async writeFile(tableName: string, table: Schema[]): Promise<void> {
		try {
			await fsPromise.writeFile(
				`${tableName}.json`,
				JSON.stringify(table, null, 2)
			);
		} catch (error) {
			console.log("Failed to save data:", error);
		}
	}

	getTable(tableName: string): {
		// getSchema: () => Schema | undefined;
		getSchema: () => { [key: string]: string | number | Date };
		getAll: () => Promise<News[]>;
		getById: (id: number) => Promise<News | undefined>;
		create: (data: News) => Promise<News>;
		update: (id: number, newData: News) => Promise<News>;
		delete: (id: number) => Promise<number>;
	} {
		return {
			getSchema: () => {
				const schema = this.schema[tableName];
				const schemaObj: { [key: string]: string | number | Date } = {};
				if (schema) {
					Object.keys(schema).forEach((key) => {
						schemaObj[key] = typeof schema[key];
					});
				}
				return schemaObj;
			},

			getAll: async () => {
				return await this.readFile(tableName);
			},

			getById: async (id: number) => {
				const table = await this.readFile(tableName);
				return table.find((item) => item.id === id);
			},

			create: async (data: News) => {
				const { title, text } = data;
				const dataId: number = Number(
					`${Math.floor(Date.now() / 1000)}${
						Math.floor(Math.random() * 100) + 1
					}`
				);
				const newItem: Schema = {
					id: dataId,
					createDate: new Date().toLocaleDateString(),
					title: title || "",
					text: text || "",
				};
				const table = await this.readFile(tableName);
				table.push(newItem);
				await this.writeFile(tableName, table);
				return newItem;
			},

			update: async (id: number, newData: News) => {
				const table = await this.readFile(tableName);
				const index = table.findIndex((record) => record.id === id);
				if (index !== -1) {
					const updatedItem = {
						...table[index],
						...newData,
					};
					table[index] = updatedItem;
					await this.writeFile(tableName, table);
					return updatedItem;
				} else {
					throw new Error(`Item with id ${id} not found.`);
				}
			},

			delete: async (id: number) => {
				const table = await this.readFile(tableName);
				const index = table.findIndex((item) => item.id === id);
				if (index !== -1) {
					const deletedId = table[index].id;
					table.splice(index, 1);
					await this.writeFile(tableName, table);
					return deletedId;
				} else {
					throw new Error(`Item with id ${id} not found.`);
				}
			},
		};
	}
}

export default FileDB;
